import React from "react";
import "./App.css";
import AppNavbar from "./components/layout/AppNavbar";

function App() {
  return (
    <div className="App">
      <AppNavbar />
      <h1>Client Panel</h1>
    </div>
  );
}

export default App;

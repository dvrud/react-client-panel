import React, { Component } from "react";

class Clients extends Component {
  render() {
    const clients = [
      {
        id: "anything",
        firstName: "Client-1",
        lastName: "lastName",
        email: "email@email.com",
        phone: "000-000-0000",
        balance: "50"
      }
    ];
    return (
      <div>
        <h1>Clients</h1>
      </div>
    );
  }
}

export default Clients;

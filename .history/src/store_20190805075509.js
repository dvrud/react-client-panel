import { createStore, combineReducers, compose } from "redux";
import firebase from "firebase";
import "firebase/firestore";
import {
  reactReduxFirebaseProvider,
  firebaseReducer
} from "react-redux-firebase";
import { createFirestoreInstance, firestoreReducer } from "redux-firestore";

// Reducers
// @todo
const firebaseConfig = {
  apiKey: "AIzaSyBErBw0pSrJWbTSiQXUB8OFGXmrEHTElx8",
  authDomain: "react-client-panel-60e29.firebaseapp.com",
  databaseURL: "https://react-client-panel-60e29.firebaseio.com",
  projectId: "react-client-panel-60e29",
  storageBucket: "react-client-panel-60e29.appspot.com",
  messagingSenderId: "813793475549",
  appId: "1:813793475549:web:f4d1707fb6226857"
};

// react-redux-firebase config
const rrfConfig = {
  userProfile: "users",
  useFirestoreForProfile: true
  // Firestore for Profile instead of Realtime DB
};

//   Init firebase instance
firebase.initializeApp(firebaseConfig);
// Init firestore
// const firestore = firebase.firestore();

// Add reactReduxFirebase enhancer when making store creator
const createStoreWithFirebase = compose(
  reactReduxFirebaseProvider(firebase, rrfConfig),
  createFirestoreInstance(firebase)
)(createStore);

const rootReducer = combineReducers({
  firebase: firebaseReducer,
  firestore: firestoreReducer
});

// Create initial state
const initialState = {};

// Create store
const store = createStoreWithFirebase(
  rootReducer,
  initialState,
  compose(
    reactReduxFirebaseProvider(firebase),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);

export default store;
